﻿using System.Data.SqlClient;

namespace WinContacts.Models
{
    /// <summary>
    /// Client for our database
    /// </summary>
    public class DatabaseClient
    {
        /// <summary>
        /// Singleton for our database
        /// </summary>
        public static DatabaseClient Client { get; } = new DatabaseClient();

        /// <summary>
        /// Connection to our database
        /// </summary>
        public SqlConnection ApplicationSqlConnection { get; } 
                                                    = new SqlConnection();

        /// <summary>
        /// Set new connectionString to our database
        /// </summary>
        /// <param name="connectionString">Connection string to database</param>
        public void SetConnectionString(string connectionString)
        {
            ApplicationSqlConnection.ConnectionString = connectionString;
        }
    }
}
