﻿using System;
using System.Collections.Generic;
using System.Linq;
using WinContacts.Tools;

namespace WinContacts.Models
{
    /// <summary>
    /// Person model is a element imitation from database
    /// </summary>
    public class Person
    {
        // mask for bad names
        private const string names_mask = "\\/;1234567890@%$#!&*?()%$:;'\"";

        /*
         * Validate @value with mask
         * @value is a string to validation
         */
        private void MaskCheck(string value)
        {
            if (value.Equals(string.Empty) || value.Contains(names_mask.ToCharArray()))
            {
                Errors.Add(new ArgumentException(
                                "Элемент содержит запрещенные символы или пуст"));
            }
        }

        /// <summary>
        /// Return person like a string implimitation
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"{Name}, {Lastname}, {Company}, {PhoneNumber}, {IMGPath}";
        }

        /// <summary>
        /// Check Equals from person
        /// </summary>
        /// <param name="obj">Another object</param>
        /// <returns>true if they are equal</returns>
        public override bool Equals(object obj)
        {
            return obj is Person && 
                   FieldsEqual(obj as Person);
        }

        /*
         * Check obj field to equal with this person
         * Param @obj is another Person type object
         * Return true if fields are equal
         */
        private bool FieldsEqual(Person obj)
        {
            return this.Name == obj.Name &&
                   this.Lastname == obj.Lastname &&
                   this.Company == obj.Company &&
                   this.PhoneNumber == obj.PhoneNumber &&
                   this.IMGPath == obj.IMGPath;
        }

        // fields of person
        private string name;
        private string lastname;
        private string company;

        // lenght of strings in fields
        private static readonly int lastname_length = 20;
        private static readonly int name_length = 10;
        private static readonly int company_length = 10;
        private static readonly int phone_length = 10;

        // exceptions list with creation errors
        private static List<Exception> Errors = new List<Exception>();

        /// <summary>
        /// Person name
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                MaskCheck(value);
                if (value.Length > name_length)
                {
                    Errors.Add(new LengthException("Слишком длинное имя"));
                    return;
                }
                name = value;
            }
        }

        
        /// <summary>
        /// Person lastname
        /// </summary>
        public string Lastname
        {
            get
            {
                return lastname;
            }

            set
            {
                MaskCheck(value);
                if (value.Length > lastname_length)
                {
                    Errors.Add(new LengthException("Слишком длинная фамилия"));
                    return;
                }
                lastname = value;
            }
        }
        
        /// <summary>
        /// Person Company
        /// </summary>
        public string Company
        {
            get
            {
                return company;
            }

            set
            {
                if (value.Contains(names_mask.ToCharArray())){
                    Errors.Add(new ArgumentException("Bad value"));
                    return;
                }
                if (value.Length > company_length)
                {
                    Errors.Add(new LengthException
                                ("Слишком длинное название компании"));
                    return;
                }
                company = value;
            }
        }

        /// <summary>
        /// Person phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Path to image
        /// </summary>
        public string IMGPath { get; set; }

        /// <summary>
        /// Create a new person with next arguments
        /// </summary>
        /// <param name="name">Persons name</param>
        /// <param name="lastname">Persons lastname</param>
        /// <param name="company">Persons company</param>
        /// <param name="phone">Persons phone number</param>
        /// <param name="photo">Path to photo image</param>
        public Person(string name, string lastname, string company, string phone, string photo)
        {
            Name = name;
            Lastname = lastname;
            Company = company;
            PhoneNumber = phone;
            IMGPath = photo ?? string.Empty;
            if (Errors.Count > 0)
            {
                throw new PersonCreateException("Ошибка при создании", Errors);
            }
        }
    }
}
