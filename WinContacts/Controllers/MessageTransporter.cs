﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WinContacts.Tools;

namespace WinContacts.Controllers
{
    /// <summary>
    /// Class to send messages from controller and some form
    /// </summary>
    public class MessageTransporter
    {
        // Connected form
        private Form form;

        // Connected Controller
        private ModelController controller;

        public MessageTransporter() { }

        /// <summary>
        /// Connect Form to receive messages from Controller
        /// </summary>
        /// <param name="form">Where we show messages</param>
        /// <param name="controller">From we get messages</param>
        public void Connect(Form form, ModelController controller)
        {
            this.form = form;
            this.controller = controller;
            this.controller.Connect(this);
        }

        /// <summary>
        /// Send error messages to form
        /// </summary>
        /// <param name="Errors">Our messages from controller</param>
        public void Answer(List<Exception> Errors)
        {
            var result = from exc in Errors
                         select exc.Message;
            if (form is IErrorShow) (form as IErrorShow).ShowErrors(result);
        }
    }
}
