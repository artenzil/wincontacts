﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using WinContacts.Models;
using WinContacts.Tools;
using System.Configuration;

namespace WinContacts.Controllers
{
    /// <summary>
    /// Fields from DataBase.
    /// Each enum element is a column name
    /// </summary>
    enum Fields : int { Name = 0, Lastname, Company, Phone, Path}

    /// <summary>
    /// Class for work with database
    /// </summary>
    class DBController : ModelController, IDisposable
    {
        //Message translator betweet form and controller
        private MessageTransporter transporter;

        //Database client
        private DatabaseClient client;

        //Connection to database
        private SqlConnection connection;

        /// <summary>
        /// Singleton Controller object
        /// </summary>
        public static DBController Instance { get; } = new DBController();

        private DBController()
        {
            transporter = null;
            client = DatabaseClient.Client;
            client.SetConnectionString(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            connection = client.ApplicationSqlConnection;
            connection.Open();
        }

        /// <summary>
        /// Connect this controller and form 
        /// </summary>
        /// <param name="newTransporter">new MessageTransporter for connection</param>
        /// <returns></returns>
        public override MessageTransporter Connect(MessageTransporter newTransporter)
        {
            MessageTransporter prev = null;
            prev = transporter;
            transporter = newTransporter;
            return prev;
        }

        /// <summary>
        /// Get a persons from database in type <list type="<Person>"> with some Parameters</list>
        /// </summary>
        /// <param name="query">SQL command to database query</param>
        /// <returns>List Person from database</returns>
        public List<Person> QueryGet(string query)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT * FROM Contacts ");
            builder.Append($"WHERE {query}");
            List<Person> peopleFromQuery = new List<Person>();
            try
            {
                CreateCommandForSQL(builder.ToString(), peopleFromQuery, connection);
            }
            catch (SqlException exc)
            {
                List<Exception> exceptions = new List<Exception>();
                Exception e = exc;
                while (e != null) {
                    exceptions.Add(e);
                    e = e.InnerException;
                    }
                transporter.Answer(exceptions);
            }
            return peopleFromQuery;
        }

        /*
         * Create command for SQL
         * Param @query is SQL query string
         * Param @peopleFromQuery is a List<Person>, where we save our objects
         * Param @connection is out connection to database
         */
        private void CreateCommandForSQL(string query, List<Person> peopleFromQuery, SqlConnection connection)
        {
            using (SqlCommand command = new SqlCommand(query)
            {
                Connection = connection
            })
            {
                ReadDataFromQuery(peopleFromQuery, command);
            }
        }

        /*
         * Read data from database
         * Param @peopleFromQuery is a List<Person> where we save our objects
         * Param @command is a SqlCommand for Execute
         */
        private void ReadDataFromQuery(List<Person> peopleFromQuery, SqlCommand command)
        {
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    peopleFromQuery.Add(PersonController.Instance.Create
                                            (reader.GetString(Fields.Name.Int()),
                                             reader.GetString(Fields.Lastname.Int()),
                                             reader.GetString(Fields.Company.Int()),
                                             reader.GetString(Fields.Phone.Int()),
                                             reader.GetString(Fields.Path.Int())));
                }
            }
        }

        /// <summary>
        /// Save person to database
        /// </summary>
        /// <param name="person">New created person</param>
        /// <returns>Success status in Boolean type</returns>
        public bool SaveToDatabase(Person person)
        {
            StringBuilder SQLExpression = new StringBuilder();
            SQLExpression.Append("INSERT INTO Contacts (Name, Lastname, Company, Phone, Path)");
            SQLExpression.Append(" VALUES (@Name, @Lastname, @Company, @Phone, @Path)");
            SqlCommand command = new SqlCommand()
            {
                Connection = connection,
                CommandText = SQLExpression.ToString(),
            };
            command.Parameters.AddWithValue("@Name", person.Name);
            command.Parameters.AddWithValue("@Lastname", person.Lastname);
            command.Parameters.AddWithValue("@Company", person.Company);
            command.Parameters.AddWithValue("@Phone", person.PhoneNumber);
            command.Parameters.AddWithValue("@Path", person.IMGPath ?? string.Empty);
            
            return ExecuteCommand(command);
        }

        /// <summary>
        /// Save new information about person
        /// </summary>
        /// <param name="changedFields">String, where contains a information about changed fields</param>
        /// <param name="keys">String, where contains a persons name and lastname</param>
        /// <returns>Success status in Boolean type</returns>
        public bool UpdatePerson(string changedFields, string keys)
        {
            StringBuilder SQLExpression = new StringBuilder();
            SQLExpression.Append("UPDATE Contacts ");
            SQLExpression.Append($" SET {changedFields} ");
            SQLExpression.Append($" WHERE {keys}");
            SqlCommand command = new SqlCommand()
            {
                Connection = connection,
                CommandText = SQLExpression.ToString(),
            };
            return ExecuteCommand(command);
        }

        /// <summary>
        /// Remove person from database
        /// </summary>
        /// <param name="key">String with persons name and lastname</param>
        /// <returns>Success status in Boolean type</returns>
        public bool RemovePerson(string key)
        {
            StringBuilder SQLExpression = new StringBuilder();
            SQLExpression.Append("DELETE FROM Contacts ");
            SQLExpression.Append($"WHERE {key}");
            SqlCommand command = new SqlCommand()
            {
                Connection = connection,
                CommandText = SQLExpression.ToString(),
            };
            return ExecuteCommand(command);
        }

        /*
         * Execute our command in database
         * Param @command is our SqlCommand to execute
         * Return @value is Success status. If we got exceptions, this code return false
         */
        private bool ExecuteCommand(SqlCommand command)
        {
            try
            {
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception exc)
            {
                List<Exception> exceptions = new List<Exception>();
                Exception e = exc;
                while (e != null) {
                    exceptions.Add(e);
                    e = e.InnerException;
                }
                transporter.Answer(exceptions);
                return false;
            }
        }

        /// <summary>
        /// Dispose for GC
        /// </summary>
        public void Dispose()
        {
            connection.Close();
        }
    }
}
