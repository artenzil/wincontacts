﻿namespace WinContacts.Controllers
{
    /// <summary>
    /// Superclass for Controllers
    /// </summary>
    public abstract class ModelController
    {
        /// <summary>
        /// <see langword="abstract"/>method to connect controller and form with MessageTransoprter
        /// </summary>
        /// <param name="newTransporter">new Message Transoprter to some form in application</param>
        /// <returns></returns>
        public abstract MessageTransporter Connect(MessageTransporter newTransporter);
    }
}
