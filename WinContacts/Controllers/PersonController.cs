﻿using WinContacts.Models;
using WinContacts.Tools;

namespace WinContacts.Controllers
{
    /// <summary>
    /// Class to controll persons create process 
    /// </summary>
    public class PersonController : ModelController
    {
        /// <summary>
        /// Noname person
        /// </summary>
        public static Person NoName { get; private set; } = new Person("NoName",
                                                                       "NoLastName",
                                                                       "Test",
                                                                       "0000000000",
                                                                       null);

        /// <summary>
        /// Singleton for use this controller in application
        /// </summary>
        public static PersonController Instance { get; } = new PersonController();

        //Message transporter to form
        private MessageTransporter transporter;

        private PersonController()
        {
            transporter = null;
        }

        /// <summary>
        /// Connect this controller and new form
        /// </summary>
        /// <param name="newTransporter">new Message transporter with some form</param>
        /// <returns>Old Message Transoprter</returns>
        public override MessageTransporter Connect(MessageTransporter newTransporter)
        {
            MessageTransporter prev = null;
            prev = transporter;
            transporter = newTransporter;
            return prev;
        }

        /// <summary>
        /// Create a new Person
        /// </summary>
        /// <param name="name">Persons name</param>
        /// <param name="lastname">Persons lastname</param>
        /// <param name="company">Persons company</param>
        /// <param name="phone">Persons phone number</param>
        /// <param name="photo">Path to photofile</param>
        /// <returns>
        /// new person with this values, or Noname person if we got some errors in process
        /// </returns>
        public Person Create(string name, string lastname, string company, string phone, string photo)
        {
            try
            {
                return new Person(name, lastname, company, phone, photo);
            }
            catch (PersonCreateException exc)
            {
                transporter.Answer(exc.ErrorList);
                return PersonController.NoName;
            }
        }
    }
}
