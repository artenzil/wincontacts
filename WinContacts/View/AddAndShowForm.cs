﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WinContacts.Models;
using WinContacts.Controllers;
using WinContacts.Tools;

namespace WinContacts.View
{
    /// <summary>
    /// Class for Add and Show Persons to (from) database
    /// </summary>
    public partial class AddAndShowForm : Form, IErrorShow
    {
        //start @Name from name field
        private string nameFieldValue;

        //start @Lastname from lastname field
        private string lastnameFieldValue;

        //start @Company from company field
        private string companyValue;

        //start @Phone from phone field
        private string phoneValue;

        //start @Path from image field
        private string photoPath;

        //current position in @People
        private int position;

        //Elements to show in form
        private List<Person> people = null;

        //Message transporter to Person controller
        private MessageTransporter transporterToPersonController;

        //Message transporter to Database controller
        private MessageTransporter transporterToDatabaseController;

        public AddAndShowForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Shows Persons in form
        /// </summary>
        /// <param name="people">Container with Persons objects</param>
        public void Show(List<Person> people)
        {
            if (people.Count == 0)
            {
                MessageBox.Show("Контакты не найдены", "Ничего нет");
                return;
            }
            position = 0;
            this.people = people;
            Show(people[position]);
        }

        /// <summary>
        /// Show Person in form
        /// </summary>
        /// <param name="person">Some person</param>
        public void Show(Person person)
        {
            SetFields(person);
            Show(false);
        }

        /// <summary>
        /// Show form in Add mode
        /// </summary>
        /// <param name="asAdd">Is form for add new person?</param>
        public void Show(bool asAdd)
        {
            SetDefaultsValues();

            this.Show();
            if (asAdd)
            {
                NextButton.Visible = false;
                PrevButton.Visible = false;
            }
            else
            {
                NameTextBox.Enabled = false;
                LastnameTextBox.Enabled = false;
                EnablePrevAndNextButtons();
            }
            this.Update();
            this.transporterToPersonController = new MessageTransporter();
            this.transporterToDatabaseController = new MessageTransporter();
            try
            {
                this.transporterToPersonController.Connect(this, PersonController.Instance);
                this.transporterToDatabaseController.Connect(this, DBController.Instance);
            }
            catch (Exception exc)
            {
                Exception e = exc;
                while(e != null)
                {
                    MessageBox.Show(e.StackTrace);
                    e = e.InnerException;
                }
            }
        }

        //Enable Prev and next Buttons
        private void EnablePrevAndNextButtons()
        {
            if (people?.Count > 1)
            {
                NextButton.Visible = true;
                PrevButton.Visible = true;
            }
        }

        /*
         * Set All textboxes to default values
         * Param @person is some Person. It can be null
         */
        private void SetFields(Person person)
        {
            if (person == null)
            {
                NameTextBox.Text = "";
                LastnameTextBox.Text = "";
                CompanyTextBox.Text = "";
                PhoneTextBox.Text = "";
            }
            else
            {
                NameTextBox.Text = person.Name;
                LastnameTextBox.Text = person.Lastname;
                CompanyTextBox.Text = person.Company;
                PhoneTextBox.Text = person.PhoneNumber;
                if (person.IMGPath != string.Empty)
                    PhotoBox.Image = Image.FromFile(person.IMGPath);
                else PhotoBox.Image = null;
            }
        }

        //Save default values to check them after edit fields
        private void SetDefaultsValues()
        {
            nameFieldValue = NameTextBox.Text;
            lastnameFieldValue = LastnameTextBox.Text;
            companyValue = CompanyTextBox.Text;
            phoneValue = PhoneTextBox.Text;
            photoPath = PhotoBox.ImageLocation;
        }

        //Is edit mode enabled
        private bool IsEdit => !(NameTextBox.Enabled && LastnameTextBox.Enabled);

        //Error messages from opersations
        private string[] errorMessages = new string[0];

        //Cancel button click event
        private void CancelButton_Click(object sender, EventArgs e)
        {
            HideForNextOperation();
        }

        //Reset fields, error messages and hide form for next operations
        private void HideForNextOperation()
        {
            errorMessages = null;
            NameTextBox.Text = "";
            LastnameTextBox.Text = "";
            CompanyTextBox.Text = "";
            PhoneTextBox.Text = "";
            PhotoBox.BackgroundImage = null;
            NameTextBox.Enabled = true;
            LastnameTextBox.Enabled = true;
            this.Hide();
        }

        //Click event to Add button
        private void AddButton_Click(object sender, EventArgs e)
        {
            Person person = PersonController.Instance.Create(NameTextBox.Text,
                                LastnameTextBox.Text,
                                CompanyTextBox.Text,
                                PhoneTextBox.SelectedText.Replace('\"', ' '),
                                PhotoBox.ImageLocation);
            if (!person.Equals(PersonController.NoName))
            {
                if (IsEdit)
                {
                    DBController.Instance.UpdatePerson(GetChangedFields(),
                                                      Keys);
                    people[position] = person;
                }
                else
                {
                    DBController.Instance.SaveToDatabase(person);
                    HideForNextOperation();
                }
            }
        }

        //Return Changed fields in Edit mode
        private string GetChangedFields()
        {
            StringBuilder builder = new StringBuilder();
            if (CompanyTextBox.Text != companyValue)
                builder.Append($"Company='{CompanyTextBox.Text}'");
            if (PhoneTextBox.Text != phoneValue)
                builder.Append($"Phone='{PhoneTextBox.Text}'");
            if (PhotoBox.ImageLocation != photoPath)
                builder.Append($"Path='{PhotoBox.ImageLocation}'");
            return builder.ToString();
        }

        //Return prepared keys to SQL query
        private string Keys => $"Name='{NameTextBox.Text}' AND Lastname='{LastnameTextBox.Text}'";

        //Click event to Picture element. Set path to new person image or null, if operation was cancelled
        private void PhotoBox_Click(object sender, EventArgs e)
        {
            FileDialog dialog = new OpenFileDialog
            {
                Title = "Выберите фото",
                InitialDirectory =
                  Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
                CheckFileExists = true,
                DefaultExt = ".bmp"
            };
            dialog.ShowDialog();
            try
            {
                PhotoBox.Image = new Bitmap(dialog.FileName);
            }
            catch (ArgumentException)
            {
                PhotoBox.Image = null;
            }
        }

        //Enable Add button
        private void EnableAddButton()
        {
            AddButton.Enabled = true;
        }

        /*
         * Change font style on some label
         * Param @label is target label
         * Param @style is a new style
         */
        private void ChangeFontStyleOnLabel (Label label, FontStyle style)
        {
            label.Font = new Font(label.Font, style);
        }

        //Name text box changed event. Change Name label style if field has new value
        private void NameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (nameFieldValue == NameTextBox.Text)
            {
                ChangeFontStyleOnLabel(NameLabel, FontStyle.Regular);
                return;
            }
            ChangeFontStyleOnLabel(NameLabel, FontStyle.Bold | FontStyle.Italic);
            if (!AddButton.Enabled) EnableAddButton();
        }

        //Lastname text box changed event. Change Lastname label style if field has new value
        private void LastnameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (lastnameFieldValue == LastnameTextBox.Text)
            {
                ChangeFontStyleOnLabel(LastnameLabel, FontStyle.Regular);
                return;
            }
            ChangeFontStyleOnLabel(LastnameLabel, FontStyle.Bold | FontStyle.Italic);
            if (!AddButton.Enabled) EnableAddButton();
        }

        //Phone text box changed event. Change Phone label style if field has new value
        private void PhoneTextBox_TextChanged(object sender, EventArgs e)
        {
            if (phoneValue == PhotoBox.Text)
            {
                ChangeFontStyleOnLabel(PhoneLabel, FontStyle.Regular);
                return;
            }
            ChangeFontStyleOnLabel(PhoneLabel, FontStyle.Bold | FontStyle.Italic);
            if(!AddButton.Enabled) EnableAddButton();
        }

        //Company text box changed event. Change Company label style if field has new value
        private void CompanyTextBox_TextChanged(object sender, EventArgs e)
        {
            if (companyValue == CompanyTextBox.Text)
            {
                ChangeFontStyleOnLabel(CompanyLabel, FontStyle.Regular);
                return;
            }
            ChangeFontStyleOnLabel(CompanyLabel, FontStyle.Bold | FontStyle.Italic);
            if (!AddButton.Enabled) EnableAddButton();
        }

        /// <summary>
        /// Show errors from operations
        /// </summary>
        /// <param name="messages">Exception messages</param>
        public void ShowErrors(IEnumerable<string> messages)
        {
            {
                StringBuilder sb = new StringBuilder();
                foreach (var msg in messages)
                {
                    sb.Append(msg + "\n");
                }
                MessageBox.Show(sb.ToString());
            }
        }

        //Click event in Remove Button. Remove person from database and people
        private void RemoveButton_Click(object sender, EventArgs e)
        {
            DBController.Instance.RemovePerson(Keys);
            people.Remove(PersonController.Instance.Create(NameTextBox.Text,
                                LastnameTextBox.Text,
                                CompanyTextBox.Text,
                                PhoneTextBox.SelectedText.Replace('\"', ' '),
                                PhotoBox.ImageLocation));
            if (people.Count == 0)
            {
                MessageBox.Show("Подходящих результатов нет", "Внимание!");
                HideForNextOperation();
            }
            else EnablePrevAndNextButtons();

        }

        //Click event in Prev Button. Change person to show  
        private void PrevButton_Click(object sender, EventArgs e)
        {
            if (position > 0)
            {
                Show(people[--position]);
            }
        }

        //Click event in Next button. Change person to show
        private void NextButton_Click(object sender, EventArgs e)
        {
            if(position < people.Count - 1)
            {
                Show(people[++position]);
            }
        }
    }
}
