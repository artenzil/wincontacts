﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WinContacts.Models;

namespace WinContacts.View
{
    /// <summary>
    /// Our main application form
    /// </summary>
    public partial class MainForm : Form
    {
        //Form for add and show person
        private AddAndShowForm AddAndShowForm;

        //Form for find persons in database
        private new Form FindForm;

        public MainForm()
        {
            InitializeComponent();
            AddAndShowForm = new AddAndShowForm();
            FindForm = new FindForm(this);
            HideForms();
        }

        //Hide all forms
        private void HideForms()
        {
            AddAndShowForm.Hide();
            FindForm.Hide();
        }

        /// <summary>
        /// Switch form from @FindForm to @AddAndShowForm
        /// </summary>
        /// <param name="people">People from database</param>
        public void SwitchForm(List<Person> people)
        {
            FindForm.Hide();
            AddAndShowForm.Show(people);
        }

        //Click event in "Exit" menu item
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Click event in "Add" menu item
        private void AddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddAndShowForm.Show(true);
        }

        //Click event in "Search" menu item
        private void SearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FindForm.Show(this);
        }

        private void MenuOnMainForm_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }
    }
}
