﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using WinContacts.Models;
using WinContacts.Controllers;
using WinContacts.Tools;

namespace WinContacts.View
{
    /// <summary>
    /// Class to Find some persons in database
    /// </summary>
    public partial class FindForm : Form, IErrorShow
    {
        // Message transoprter to database controller
        private MessageTransporter transporter;

        //Link to MainForm object
        private MainForm mainForm;

        //Query result
        public List<Person> GetResult { get; private set; }

        public FindForm(MainForm main)
        {
            InitializeComponent();
            GetResult = new List<Person>();
            transporter = new MessageTransporter();
            transporter.Connect(this, DBController.Instance);
            mainForm = main;
        }

        //Click event to Cancel button. Hide this form
        private void CancelButton_Click(object sender, EventArgs e)
        {
            ResetFields();
            this.Hide();
        }

        // Reset all fields to defaults
        private void ResetFields()
        {
            NameTextBox.Text = "";
            LastnameTextBox.Text = "";
            CompanyTextBox.Text = "";
            PhoneTextBox.Text = "";
        }

        /*
         * Click event in Find Button
         * Generate query to get persons from database with any parameters
         */
        private void FindButton_Click(object sender, EventArgs e)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(QueryValueFromField(Fields.Name, NameTextBox.Text));
            builder.Append(QueryValueFromField(Fields.Lastname, 
                                        LastnameTextBox.Text));
            builder.Append(QueryValueFromField(Fields.Company,
                                        CompanyTextBox.Text));
            builder.Append(QueryValueFromField(Fields.Phone,
                                                $"\"{PhoneTextBox.SelectedText}\""));
            GetResult = DBController.Instance.QueryGet(builder.ToString());
            if (GetResult == null)
            {
                MessageBox.Show("Something wrong");
            }
            ResetFields();
            mainForm.SwitchForm(GetResult);
        }

        /*
         * Generate strint to Sql query
         * Param @id is column name in database
         * Param @text is text from field
         * Return @string with column=value if field is not empty
         */
        private string QueryValueFromField(Fields id, string text)
        {
            text = text.Replace('\"', ' ');
            if(text.Trim() != string.Empty)
            {
                return $"{id}=\'{text.Trim()}\' ";
            }
            return string.Empty;
        }

        /// <summary>
        /// Show errors from Query proccess
        /// </summary>
        /// <param name="messages">Inner Exceptions messages</param>
        public void ShowErrors(IEnumerable<string> messages)
        {
            {
                StringBuilder sb = new StringBuilder();
                foreach (var msg in messages)
                {
                    sb.Append(msg + "\n");
                }
                MessageBox.Show(sb.ToString());
            }
        }
    }
}
