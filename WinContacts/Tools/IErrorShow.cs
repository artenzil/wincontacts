﻿using System.Collections.Generic;

namespace WinContacts.Tools
{
    /// <summary>
    /// Interface to show some error messages
    /// </summary>
    public interface IErrorShow
    {
        /// <summary>
        /// Show some error messages
        /// </summary>
        /// <param name="messages">Exception messages</param>
        void ShowErrors(IEnumerable<string> messages);
    }
}
