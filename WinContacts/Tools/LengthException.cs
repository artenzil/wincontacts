﻿using System;

namespace WinContacts.Tools
{
    /// <summary>
    /// Extension class. Used by show user a bad element lenght
    /// </summary>
    class LengthException : Exception
    {
        public LengthException(string message) : base(message)
        {
        }
    }
}
