﻿using WinContacts.Controllers;

namespace WinContacts.Tools
{
    /// <summary>
    /// Extension class to conver enum field to int value
    /// </summary>
    static class EnumToIntExtension
    {
        /// <summary>
        /// Convert enum field to int implimentation
        /// </summary>
        /// <param name="f">Enum element</param>
        /// <returns>Int value</returns>
        public static int Int(this Fields f)
        {
            return (int)f;
        }
    }
}
