﻿using System;
using System.Collections.Generic;

namespace WinContacts.Tools
{
    /// <summary>
    /// Class exception. Used for show error in person creation process
    /// </summary>
    class PersonCreateException : Exception
    {
        /// <summary>
        /// List of inner exceptions
        /// </summary>
        public List<Exception> ErrorList { get; private set; } = new List<Exception>();

        public PersonCreateException(string message, List<Exception> errors) 
            : base(message)
        {
            ErrorList = errors;
        }
    }
}
