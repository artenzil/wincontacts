﻿using System.Linq;

namespace WinContacts.Tools
{
    /// <summary>
    /// String extension class. Used for add new functional to <see cref="System.String.Contains(string)"/>
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Check string to contains some elements in char array
        /// </summary>
        /// <param name="str">our string</param>
        /// <param name="mask">char array for check</param>
        /// <returns>true if our string contains any char from mask</returns>
        public static bool Contains(this string str, char[] mask)
        {
            foreach (var c in mask)
            {
                if (str.Contains(c))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
